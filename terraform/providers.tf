terraform {
  required_providers {
    spotify = {
      version = "~> 0.2.6"
      source  = "conradludgate/spotify"
    }
  }
}

provider "spotify" {
  auth_server = var.auth_server
  api_key = var.spotify_api_key
  username = var.user
  token_id = var.token
}
