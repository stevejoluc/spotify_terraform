resource "spotify_playlist" "playlist" {
  name        = "Terraform Playlist"
  description = "This playlist was created with Terraform Cloud"
  public      = true

  tracks = flatten([
    data.spotify_search_track.old_dominion.tracks[*].id,
    data.spotify_search_track.luke_combs.tracks[*].id,
    data.spotify_search_track.thomas_rhett.tracks[*].id,
    data.spotify_search_track.jason_aldean.tracks[*].id,
    data.spotify_search_track.russell_dickerson.tracks[*].id,
    data.spotify_search_track.eric_church.tracks[*].id,
    data.spotify_search_track.kenny_chesney.tracks[*].id,
    data.spotify_search_track.luke_bryan.tracks[*].id,
  ])
}

# output "tracks" {
#   value = data.spotify_search_track.by_artist.tracks
# }
