variable "spotify_api_key" {
  type = string
}

variable "token" {
  type = string
}

variable "user" {
  type = string
}

variable "auth_server" {
  type = string
}