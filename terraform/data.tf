data "spotify_search_track" "old_dominion" {
  artist = "Old Dominion"
  limit = 30
}

data "spotify_search_track" "luke_combs" {
  artist = "Luke Combs"
  limit = 10
}

data "spotify_search_track" "thomas_rhett" {
  artist = "Thomas Rhett"
  limit = 10
}

data "spotify_search_track" "jason_aldean" {
  artist = "Jason Aldean"
  limit = 2
}

data "spotify_search_track" "russell_dickerson" {
  artist = "Russell Dickerson"
  limit = 2
}

data "spotify_search_track" "eric_church" {
  artist = "Eric Church"
  limit = 5
}

data "spotify_search_track" "kenny_chesney" {
  artist = "Kenny Chesney"
  limit = 10
}

data "spotify_search_track" "luke_bryan" {
  artist = "Luke Bryan"
  limit = 1
}
